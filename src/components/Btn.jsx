import React from 'react';

export default class Btn extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {valeur: 0 };
  }
  incre() {
    this.setState(state => ({
      valeur: this.state.valeur + 1
    }));
  }

  render (){
    
    return (
      <div>
        Compteur: {this.state.valeur}
        <button className="btn1" onClick={ this.incre()}> 
          Increment me
        </button>
        <button className="btn2" onClick={this.incre()}> 
          decrement me
        </button>
      </div>
      
    );
  }
}
