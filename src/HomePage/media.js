import React from 'react';
import PropTypes from 'prop-types';
import Modal from './Modal';

class media extends React.Component {
    constructor(props, context) {
        super(props, context);
        this.mouseOver = this.mouseOver.bind(this);
        this.mouseOut = this.mouseOut.bind(this);
        this.handleShow = this.handleShow.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.state = {
            isOpen: false,
            hover: false,
            show: false
        };
    }
    toggleModal = () => {
        this.setState({
            isOpen: !this.state.isOpen
        });
    }
    mouseOver = () => {
        this.setState({ hover: true });
    }

    mouseOut() {
        this.setState({ hover: false });
    }

    handleClose() {
        this.setState({ show: false });
    }

    handleShow() {
        this.setState({ show: true });
    }

    render() {


        return (
            <div className="container ">

                <img src={require('../assets/index.jpeg')} className="image" onClick={this.handleShow}>
                </img>
                <div className="inner transform">
                    <div className="text">
                        12
                            <svg id="i-heart" viewBox="0 0 32 32" width="25" height="20" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                            <path d="M4 16 C1 12 2 6 7 4 12 2 15 6 16 8 17 6 21 2 26 4 31 6 31 12 28 16 25 20 16 28 16 28 16 28 7 20 4 16 Z" />
                        </svg>
                        340
                            <svg id="i-msg" viewBox="0 0 32 32" width="25" height="20" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                            <path d="M2 4 L30 4 30 22 16 22 8 29 8 22 2 22 Z" />
                        </svg>

                    </div>
                </div>
                <Modal show={this.state.show} onHide={this.handleClose}>
                    <div>

                        <img src={require("../assets/index.jpeg")}></img>
                    </div>

                    <div >
                        <button onClick={this.handleClose}>
                            Close
                            </button>

                    </div>
                </Modal>
            </div>
        );
    }
}



export default media;