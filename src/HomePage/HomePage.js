import React, { Component } from 'react';
import axios from 'axios';
import logo from '../logo.svg';

import Modal from './Modal';
import Media from './media';
import Btn from '../components/Btn';
import "../css/style.css";



class HomePage extends React.Component {
  constructor(props) {
    super(props);
    this.mouseOver = this.mouseOver.bind(this);
    this.mouseOut = this.mouseOut.bind(this);
    this.state = {
      persons:null,
      isOpen: false,
      hover: false
    }
  }
  componentDidMount() {
    axios.get('https://jsonplaceholder.typicode.com/users/1')
      .then(res => {
        const persons = res.data;
        console.log(persons);
        this.setState({ persons });
      })
  }
  toggleModal = () => {
    this.setState({
      isOpen: !this.state.isOpen
    });
  }
  mouseOver = () => {
    this.setState({hover: true});
  }
  mouseOut() {
      this.setState({hover: false});
  }
// <ul>{this.state.persons.map(person => <li>{person.name}</li>)}</ul>

  render() {

    const { modalOpen } = this.state;

    return (
      <div>
          <Header userprop={this.state.persons} />
          
          <Content />
        
      </div>

    );
  }
}

class Header extends React.Component {

  render() {
    return (
      <div className='rowC'>
        <div ><img className="roundedImage" src={require("../assets/pro2.jpeg")} onClick={this.toggleModal}></img> </div>
        <div >
          <h1>{this.props.userprop?this.props.userprop.username:""} </h1>
          <p>
            Dwayne Johnson,  né le 2 mai 1972 à Hayward. Issu d'une famille
            de lutteurs, il devient lui aussi catcheur.
          </p>
        </div>
        <div >
          <button> Follow </button>
          <ul>
            <li>{this.props.userprop?this.props.userprop.id:""} Publications</li>
            <li>1513 abonnés</li>
            <li>1513 abonnements</li>
          </ul>
        </div>

      </div>
    );
  }
}

class Content extends React.Component {
 
  render() {
    return (
      <div className="albumV">

        <Media></Media>
        <Media></Media>
        <Media></Media>
        <Media></Media>
        <Media></Media>
        <Media></Media>
        <Media></Media>
        <Media></Media>
        

      </div>
    );
  }
}

class Vignette extends React.Component {
  
  render() {
    return (
      <div className="container">

        <img src={require('../assets/index.jpeg')} className="image">
        </img>
        <div className="middle">
            <div className="text">
             12
             <svg id="i-heart" viewBox="0 0 32 32" width="25" height="20" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                <path d="M4 16 C1 12 2 6 7 4 12 2 15 6 16 8 17 6 21 2 26 4 31 6 31 12 28 16 25 20 16 28 16 28 16 28 7 20 4 16 Z" />
            </svg>
            340
            <svg id="i-msg" viewBox="0 0 32 32" width="25" height="20" fill="none" stroke="currentcolor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2">
                <path d="M2 4 L30 4 30 22 16 22 8 29 8 22 2 22 Z" />
            </svg>
           
          </div>
        </div>
      </div>
    );
  }
}


export { HomePage };






